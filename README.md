# ublock-list

personnal filters for ublock origin

How to use
----------

Add the following line to your own list in ublock origin :
```
https://gitlab.com/cgrand221/ublock-list/-/raw/master/list.txt
```

LICENCE
-------

```
https://gitlab.com/cgrand221/ublock-list/-/raw/master/LICENSE
```


